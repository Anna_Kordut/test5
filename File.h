#pragma once
#include <iostream>
class File
{
private:
	int size;
	std::string name;
    std::string format;
public:
    File();
    int getSize();
    void setSize(int size);
    std::string getName();
    void setName(std::string name);
    std::string getFormat();
    void setFormat(std::string format);

};