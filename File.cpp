#include "File.h"


int File::getSize() {
    return size;
}

File::File()
{
    name = "";
    size = 0;
    format="";
}

void File::setSize(int size) {
    this->size = size;
}

std::string File::getName() {
    return name;
}

void File::setName(std::string name) {
    this->name = name;
}

std::string File::getFormat() {
    return format;
}

void File::setFormat(std::string format) {
    this->format = format;
}
